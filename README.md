### Software Development Engineer in Test Interview

#### Task 
Using a retail website (Amazon, Takealot, Flipkart etc etc)
User should be able to add a product to the basket and verify the correct product is added

#### Objectives
- Create feature file using Gherkin syntax
- Create step definitions and link these to the feature file
- Write implementation which links both pieces up
- Create TestRunner to run test 
- Make sure the test passes

-	Automate a user adding an item to their basket and then verify that the item has been added to the basket
-	Acceptance Criteria
	- An automation framework
	- May use any programming language and any automation tooling (non-commercial)
	- Uses any online retail platform e.g. Amazon, Takealot, Woolworths
	- No authentication to the retail platform is required
	- Test executes successfully



Good Luck!