Feature: Test functionality of adding items to basket

Scenario: Add single item to basket and validate totals

Given User is on the search page
When User search and add 'Sky97 Micro drone' to the basket
Then Correct item and totals should be visible in the basket


