
from features.steps.pages.base_page import BasePage


class SearchPage(BasePage):
    URL = "/"
    search_input_field = '[name=search]'
    search_reault = ""

    def __init__(self, context):
        self.browser = context.browser
        self.context = context
        
        
    def search_for_item(self, search_term):
        self.fill_field(self.search_input_field, search_term)
        self.browser.page.keyboard.press('Enter')
        

    def add_item_to_basket(self, item):
        pass
    
    # Add first iten in sear result
    def get_item_parameters(self):
        self.browser.page.wait_for_selector("button:has-text('Add to cart')").click()

    
    def search_and_add_item_to_basket(self, search_term):
        self.search_for_item(search_term)
        item = self.get_item_parameters()
        self.add_item_to_basket(item)
        
        
        # validate item and the total in the cart 
    def validate_cart(self,context):
        self.context.browser.goto("http://takealot.com/cart")
           
    
        
    
        