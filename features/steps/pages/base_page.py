from browser import browser
from browser.browser import Browser


class BasePage(Browser):
    URL = ""
    timeout=30000


    def __init__(self, context,browser):
        self.browser = context.browser
        self.timeout = context.timeout
        self.context = context


    def load(self):
        self.browser.goto(f"{self.context.base_url}{self.URL}")
        self.context.browser.wait_for_url_to_contain(self.URL)

    def wait_for_url_to_contain(self, text):
        self.context.browser.wait_for_url_to_contain(text)

