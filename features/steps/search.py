from browser.browser import Browser as page
from pages.search_page import SearchPage
from behave import given, when, then


@given("User is on the search page")
def given_user_on_search_age(context):
    search_page= SearchPage(context)
    search_page.load()


@when("User search and add '{search_term}' to the basket")
def when_user_search_and_add_to_basket(context, search_term):
    search_page= SearchPage(context)
    search_page.search_and_add_item_to_basket(search_term)
    


@then("Correct item and totals should be visible in the basket")
def then_correct_item_add_to_basket(context, search_term):
    search_page= SearchPage(context)
    search_page.validate_cart()
    