from browser.browser import Browser
from features.steps.pages.base_page import BasePage

from random import randrange
import json
import datetime



page = None
browser: Browser = None
browserName = "TestProject"
browserName = "Playwright"
data_setup = None
test_data = None

ENV_DATA = {
    "test": {
        "base_url": "http://takealot.com",
    },
    "staging": {
        "base_url": "",
    },
}


def before_all(context):
    # Get configs and set defaults
    context.env = context.config.userdata.get("env", "test")
    context.env_data = ENV_DATA[context.env]
    context.base_url = context.env_data["base_url"]
    context.timeout = 30000

    
    browser_options = {
        "headless": True
        if context.config.userdata.get("headless") == "true"
        else False,
        "timeout": context.timeout,
    }

    context.browser = Browser(browserName, browser_options)
    context.page, context.browser_context = context.browser.launch(context.base_url)
    context.browser_context = context.browser.browser_context.contexts[0]
    context.browser_context.set_default_timeout(context.timeout)

    context.base_page = BasePage(context, context.browser)


def before_feature(context, feature):
    pass


def before_step(context, step):
    pass


def after_step(context, step):
    pass


def before_scenario(context, scenario):
    pass


def after_scenario(context, scenario):
    pass


def after_all(context):
    try:
        close_browser(context)
    except Exception:
        print("Error closing browser")


def close_browser(context):
    context.browser.close()

