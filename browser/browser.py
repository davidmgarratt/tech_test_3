
from browser.playwright import Playwright
import time



class Browser():
    browser = None
    page = None

    def __init__(self, framework, options):
        self.timeout = int(options.get("timeout", 30000))
        self.framework = framework

        if framework == "Playwright":
            pw = Playwright(options)
            self.browser = pw

    def execute_script(self, script):
        return self.page.evaluate(script)

    def launch(self, url=""):
        self.page, self.browser_context = self.browser.launch(url=url)
        return self.page, self.browser_context

    def close(self):
        self.browser.close()

    def goto(self, url):
        if self.current_url() != url:
            self.browser.goto(url, timeout=self.timeout)

    def refresh(self):
        self.browser.refresh()

    def wait_for_url_to_contain(self, text):
        self.browser.wait_for_url_to_contain(text)
        self.browser.wait_for_load_state("networkidle")

    def wait_for_url_to_be(self, text):
        self.browser.wait_for_url_to_be(text)
        self.browser.wait_for_load_state("networkidle")
        
    def url_contains(self, url):
        self.browser.url_contains(url)

    def current_url(self):
        return self.browser.current_url()

    def type_in_field(self, el, text):
        self.browser.type_in_field(el, text)

    def fill_field(self, el, text, clear=True):
        self.wait_for_element_visible(el)
        self.browser.fill_field(el, text, clear)

    def click(self, el, **kwargs):
        parent = kwargs.pop("parent", "")
        el = f"{parent}{el}"
        # self.wait_for_selector(el, **kwargs)
        self.browser.click(el, **kwargs)

    def click_and_return_response(self, el, match="", **kwargs):
        parent = kwargs.get("parent", "")
        el = f"{parent}{el}"
        return self.browser.click_and_return_response(el, match)

    def click_link_with_text(self, text):
        self.browser.click_link_with_text(text)

    def click_button_with_text(self, text):
        self.browser.click_button_with_text(text)

    def click_on_text(self, text, parent=""):
        el = f"{parent}//*[text()='{text}']"
        self.browser.click(el)

    def select_option(self, select, value):
        self.browser.select_option(select, value)

    def see_text(self, text, parent=""):
        self.browser.see_text(text, parent)

    def see_element_text_equals(self, text, el):
        return self.browser.see_element_text_equals(text, el)

    def see_element_has_text(self, text, el):
        return self.browser.see_element_has_text(text, el)

    def get_text(self, el):
        return self.browser.get_text(el)

    def get_value(self, el):
        return self.browser.get_value(el)

    def element_is_disabled(self, el):
        self.browser.element_is_disabled(el)

    def element_is_not_visible(self, el, parent=""):
        el = f"{parent}{el}"
        return self.browser.element_is_not_visible(el)

    def wait_for_element_visible(self, el, **kwargs):
        parent = kwargs.get("parent", "")
        timeout = kwargs.get("timeout", self.timeout)
        return self.browser.wait_for_element_visible(f"{parent}{el}", timeout=timeout)

    def element_is_visible(self, el, parent=""):
        el = f"{parent}{el}"
        return self.browser.element_is_visible(el)

    def element_with_text_is_visible(self, el, text, parent=""):
        el = f"{parent}{el}"

        if el.endswith("]"):
            el = f"{el.rstrip(']')} and text() = '{text}']"
        else:
            el = f"{el}[text() = '{text}']"

        self.browser.wait_for_element_visible(el)

    def see_element(self, el):
        self.browser.element_is_visible(el)

    def wait_for_elements_not_visible(self, el):
        self.browser.wait_for_elements_not_visible(el)

    def wait_for_element_not_visible(self, el):
        self.browser.wait_for_element_not_visible(el)


    def wait_for_load_state(self, state):
        self.browser.wait_for_load_state(state)

    def after_step(self, context, step):
        self.browser.after_step(context, step)

    def after_scenario(self, context, scenario):
        self.browser.after_scenario(context, scenario)

    def delete_all_cookies(self):
        return self.browser.delete_all_cookies()

    def add_cookie(self, cookie):
        return self.browser.add_cookie(cookie)

    def select_from_dropdown(self, el, text):
        self.browser.select_from_dropdown(el, text)

    def get_element_count(self, el):
        els = self.browser.find_elements(el)
        return len(els)

    def get_elements(self, el):
        els = self.browser.find_elements(el)
        return els

    # local storage
    def remove_item_from_local_storage(self, key):
        self.browser.remove_item_from_local_storage(key)

    def add_item_to_local_storage(self, key, value):
        self.browser.add_item_to_local_storage(key, value)

    def is_item_present_in_local_storage(self, key):
        return self.browser.is_item_present_in_local_storage(key)

    def get_item_from_local_storage(self, key):
        return self.browser.get_item_from_local_storage(key)

    def clear_local_storage(self):
        self.browser.clear_local_storage()
        time.sleep(1)

    # helpers
    def find_elements(self, el):
        return self.page.query_selector_all(el)
