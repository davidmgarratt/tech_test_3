from playwright.sync_api import sync_playwright
from playwright.sync_api import expect


class Playwright:
    def __init__(self, options={}):

        self.headless = options.get("headless", False)
        self.timeout = int(options.get("timeout", 30000))

    def after_scenario(self, context, scenario):
        pass

    # Session
    def launch(self, url=""):
        playwright = sync_playwright().start()
        self.browser = playwright.chromium.launch(
            headless=self.headless, timeout=self.timeout
        )

        self.page = self.browser.new_page(viewport={"width": 1366, "height": 768})
        self.page.goto(url, timeout=self.timeout)
        return self.page, self.browser

    def close(self):
        self.browser.close()

    def delete_all_cookies(self):
        for context in self.browser.contexts:
            context.clear_cookies()

        return True

    def add_cookie(self, cookie):
        result = self.browser.contexts[0].add_cookies([cookie])
        self.refresh()
        return result

    def current_url(self):
        return self.page.url

    def refresh(self):
        self.page.reload()

    #  Navigate

    def goto(self, url, **kwargs):
        self.page.goto(url, **kwargs)

    def fill_field(self, element, text, clear=True):
        self.page.fill(element, text)

    # Click
    def click(self, el, **kwargs):
        locator = self.page.locator(el)
        locator.click()

    def click_and_return_response(self, el, match="**", **kwargs):
        with self.page.expect_response(match) as reponse_info:
            self.page.click(el)
        r = reponse_info.value
        return r

    # Select options
    def select_option(self, select, value):
        self.page.select_option(select, value)

    # getters
    def get_text(self, el):
        return self.page.inner_text(el)

    def get_value(self, el):
        return self.page.get_attribute(el, "value")

    def is_item_present_in_local_storage(self, key):
        return self.page.evaluate(f"window.localStorage.getItem('{key}');") != None

    def clear_local_storage(self):
        self.page.evaluate("window.localStorage.clear();")

    # # assert
    def see_text(self, text, parent=""):
        el = f'{parent}//*[text()="{text}"]'
        assert (
            self.page.inner_text(el, timeout=self.timeout) == text
        ), f"Expected to see {text} in element: {el}"

    def see_element_text_equals(self, text, el):
        self.page.wait_for_selector(f'{el}[text()="{text}"]', timeout=5000)

    def see_element_has_text(self, text, el):
        assert self.page.is_visible(
            f'{el}//*[text()="{text}"]', timeout=2000
        ), f'Expected {el} to have text "{text}" be visible'

    def element_is_not_visible(self, el):
        assert self.page.is_hidden(el, timeout=2000), f"Expected {el} not to be visible"

    def element_is_visible(self, el, **kwargs):
        # locator = self.get_locator(el, **kwargs)
        _el = self.page.is_visible(el, timeout=5000)
        assert _el, f"Expected {el} to be visible"
        return _el

    def wait_for_url_to_contain(self, url):
        self.page.wait_for_url(f"**{url}**", timeout=self.timeout)
        
    def wait_for_url_to_be(self, url):
        self.page.wait_for_url(f"**{url}", timeout=self.timeout)

    def wait_for_load_state(self, state):
        self.page.wait_for_load_state(state=state, timeout=self.timeout)

    def url_contains(self, url):
        assert url in self.page.url, f"Expected url to contain {url}"

    # # helpers
    def find_element(self, el, **kwargs):
        locator = self.get_locator(el, **kwargs)
        return locator

    def find_elements(self, el):
        return self.page.query_selector_all(el)

    def wait_for_elements_not_visible(self, el, **kwargs):
        locators = self.page.locator(el)
        for i in range(0, locators.count()):
            timeout = kwargs.get("timeout", self.timeout)
            locators.nth(i).wait_for(state="hidden", timeout=timeout)

    def wait_for_element_not_visible(self, el, **kwargs):
        locator = self.get_locator(el, state="hidden")
        locator.is_hidden()

    def wait_for_element_visible(self, el, **kwargs):
        locator = self.get_locator(el, state="visible", **kwargs)
        return locator

    def get_locator(self, el, **kwargs):
        state = kwargs.get("state", "visible")
        timeout = kwargs.get("timeout", self.timeout)
        locator = self.page.locator(el)
        locator.wait_for(timeout=timeout, state=state)
        return locator
